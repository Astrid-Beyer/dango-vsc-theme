# 🍡 Dango Theme

![](images/palette.png)

**Dango Theme** — A vibrant, minimalistic & colorful VSCode dark theme! Made with love, it's my first theme.

## Download

Find it on the Visual Studio Marketplace — [link](https://marketplace.visualstudio.com/items?itemName=teloru.dango-theme)

Or in **Extensions** in VS Code:

1. Open the **Extensions** sidebar in VS Code
2. Search for `dango theme` (or `Teloru`)
3. Click **Install**
4. Open the **Command Palette** with `Ctrl+Shift+P` or `⇧⌘P`
5. Select **Preferences: Color Theme** and choose "Dango theme"
6. Enjoy!

## Screenshots

![](images/capture_1.png)

![](images/capture_2.png)

## Stay in touch

You can find me there:

- [Twitch](https://twitch.com/teloru)
- [GitLab](https://gitlab.com/Astrid-Beyer)
- [Ko-fi](http://ko-fi.com/teloru/)

Happy Coding!
